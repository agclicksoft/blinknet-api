'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddAdressToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table
      .integer('adress_id')
      .unsigned()
      .references('id')
      .inTable('adresses')
      .onUpdate('CASCADE')
    })
  }

  down () {
    this.table('users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddAdressToUserSchema
