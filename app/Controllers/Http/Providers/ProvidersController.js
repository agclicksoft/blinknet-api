'use stric'

const Provider = use('App/Models/Provider')
const User = use('App/Models/User')
const Category = use('App/Models/Category')
const OperatingRegion = use('App/Models/OperatingRegion')
const Brand = use('App/Models/Brand')
const Adress = use('App/Models/Adress')

class ProvidersController {
    async store({ request, auth, response}) {
        const body = request.post()
        const user = await auth.getUser()
        const provider = await user.provider().create({})
        user.merge({id: body.id, email: body.email, cel: body.cel, name: body.name, cnpj: body.cnpj, company_name: body.company_name })
        user.currente_role = 'provider'
        await user.save()
        var category
        for (const iterator of body.provider.categories) {
            category = await Category.findOrCreate({ name: iterator.name })
            await provider.categories().attach([category.id])
        }
        var brand
        for (const iterator of body.provider.brands) {
            brand = await Brand.findOrCreate({ name: iterator.name })
            await provider.brands().attach([brand.id])
        }        
        var adress
        for (const i of body.provider.adresses) {
            adress = await Adress.findOrCreate({
               parents: 'Brasil', state: i.state, city: i.city, neighborhood: i.neighborhood,
                //number: i.number, complement: i.complement, street: i.street
            })
             const operatingRegions = await provider.operatingRegions().create({})
             
             await operatingRegions.adress().associate(adress)
        }
        const account = await User.query()
        .where('id', user.id)
        .with('provider', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('corporate', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('buyer', (builder) => {
          builder.with('categories')
        })
        .first()

        response.status(200).send({
            message: 'Dados atualizados com sucesso!',
            data: account
        })
    }

    async update({ request, response, params }) {
        const body = request.post()
        const provider = await Provider.findOrFail(params.id)
        const user = await provider.user().fetch()
        user.merge({ email: body.email, cel: body.cel, name: body.name, cnpj: body.cnpj, company_name: body.company_name })
        user.currente_role = 'provider'
        await user.save()
        var category
        for (const iterator of body.provider.categories) {
            category = await Category.findOrCreate({ name: iterator.name })
            await provider.categories().attach([category.id])
        }
        var brand
        for (const iterator of body.provider.brands) {
            brand = await Brand.findOrCreate({ name: iterator.name })
            await provider.brands().attach([brand.id])
        }
        await OperatingRegion.query().where('provider_id', provider.id).delete()
        var adress
        for (const i of body.provider.adresses) {
            adress = await Adress.findOrCreate({
               parents: 'Brasil', state: i.state, city: i.city, neighborhood: i.neighborhood,
                //number: i.number, complement: i.complement, street: i.street
            })
             const operatingRegions = await provider.operatingRegions().create({})
             
             await operatingRegions.adress().associate(adress)
        }
        const account = await User.query()
        .where('id', user.id)
        .with('provider', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('corporate', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('buyer', (builder) => {
          builder.with('categories')
        })
        .first()

        response.status(200).send({
            message: 'Dados atualizados com sucesso!',
            user: account
        })
    }
}

module.exports = ProvidersController