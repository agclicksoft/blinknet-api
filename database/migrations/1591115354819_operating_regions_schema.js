'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class OperatingRegionsSchema extends Schema {
  up () {
    this.create('operating_regions', (table) => {
      table.increments()
      table
      .integer('provider_id ')
      .unsigned()
      .references('id')
      .inTable('providers')
      .onUpdate('CASCADE')
      table.string('name', 254)
      table.timestamps()
    })
  }

  down () {
    this.drop('operating_regions')
  }
}

module.exports = OperatingRegionsSchema
