'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CorporatesBrandsSchema extends Schema {
  up () {
    this.create('corporates_brands', (table) => {
      table.increments()
      table
      .integer('corporate_id')
      .unsigned()
      .references('id')
      .inTable('corporates')
      .onUpdate('CASCADE')  
      table
      .integer('brand_id')
      .unsigned()
      .references('id')
      .inTable('brands')
      .onUpdate('CASCADE')  
      table.timestamps()
    })
  }

  down () {
    this.drop('corporates_brands')
  }
}

module.exports = CorporatesBrandsSchema
