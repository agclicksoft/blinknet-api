'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ActivityFieldsSchema extends Schema {
  up () {
    this.create('activity_fields', (table) => {
      table.increments()
      table.string('name', 254)
      table.timestamps()
    })
  }

  down () {
    this.drop('activity_fields')
  }
}

module.exports = ActivityFieldsSchema
