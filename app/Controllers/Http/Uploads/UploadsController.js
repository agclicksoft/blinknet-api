'use strict'

const Drive = use('Drive')
const Image = use('App/Models/Image')

class UploadsController {

    //uploadImage
    async uploadImage({ request }) {
        // upload rules
        console.log(request)
        const validationOptions = {
            types: ['jpeg', 'jpg', 'png', 'pdf'],
            size: '5mb'
        }
        var images
        request.multipart.file('image', validationOptions, async file => {
            console.log('cheguei aqui 3')
            // set file size from stream byteCount, so adonis can validate file size
            file.size = file.stream.byteCount

            // catches validation errors, if any and then throw exception
            const error = file.error()
            if (error.message) {
                throw new Error(error.message)
            }
            const Key = `uploads/${(Math.random() * 100).toString()}-${file.clientName}`
            const ContentType = file.headers['content-type']
            const ACL = 'public-read'
            // upload file to s3
            const path = await Drive.put(Key, file.stream, {
                ContentType,
                ACL
            })

            images = await Image.create({
                path,
                key: Key
            })
        })
        await request.multipart.process()
        return images
    }
}

module.exports = UploadsController