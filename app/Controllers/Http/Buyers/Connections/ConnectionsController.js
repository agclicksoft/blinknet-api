'use strict'

const Connection = use('App/Models/Connection')
const Buyer = use('App/Models/Buyer')

class ConnectionsController {

    async index({params, auth}) {
        const buyer  = await Buyer.findOrFail(params.id)
        const user = await auth.getUser()  
      
        
        const connections = await Connection.query()
        .where({buyer_id : buyer.id})
      
        .with('provider', (builder) => {
            builder.with('user', (builder) => {
                builder.with('image')
            })
        })
        .with('buyer', (builder) => {
            builder.with('user', (builder) => {
                builder.with('image')
            })
        })
        .fetch()

        return connections
    }
}

module.exports = ConnectionsController