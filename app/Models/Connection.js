'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Connection extends Model {
    provider() {
        return this.belongsTo('App/Models/Provider')
    }
    buyer() {
        return this.belongsTo('App/Models/Buyer')
    }
}

module.exports = Connection
