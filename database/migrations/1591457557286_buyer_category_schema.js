'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BuyerCategorySchema extends Schema {
  up () {
    this.create('buyer_categories', (table) => {
      table.increments()
      table
      .integer('buyer_id')
      .unsigned()
      .references('id')
      .inTable('buyers')
      .onUpdate('CASCADE')  
      table
      .integer('category_id')
      .unsigned()
      .references('id')
      .inTable('categories')
      .onUpdate('CASCADE')  
      table.timestamps()
    })
  }

  down () {
    this.drop('buyer_categories')
  }
}

module.exports = BuyerCategorySchema
