'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductsCategoriesSchema extends Schema {
  up () {
    this.create('products_categories', (table) => {
      table.increments()
      table
      .integer('product_id')
      .unsigned()
      .references('id')
      .inTable('products')
      .onUpdate('CASCADE')  
      table.string('name', 254)
      table.timestamps()
    })
  }

  down () {
    this.drop('products_categories')
  }
}

module.exports = ProductsCategoriesSchema
