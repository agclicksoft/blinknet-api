'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddCotraintsUserProfileSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table
      .integer('image_id')
      .unsigned()
      .references('id')
      .inTable('images')
      .onUpdate('CASCADE')
      .after('id')
    })
  }

  down () {
    this.table('users', (table) => {
      table.dropCollumn('image_id')
    })
  }
}

module.exports = AddCotraintsUserProfileSchema
