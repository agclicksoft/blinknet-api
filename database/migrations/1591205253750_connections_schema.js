'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ConnectionsSchema extends Schema {
  up () {
    this.create('connections', (table) => {
      table.increments()
      table
      .integer('provider_id')
      .unsigned()
      .references('id')
      .inTable('providers')
      .onUpdate('CASCADE')
      table
      .integer('buyer_id')
      .unsigned()
      .references('id')
      .inTable('buyers')
      .onUpdate('CASCADE')
      table
      .integer('corporate_id')
      .unsigned()
      .references('id')
      .inTable('corporates')
      .onUpdate('CASCADE')
      table.boolean('accept')
      table.timestamps()
    })
  }

  down () {
    this.drop('connections')
  }
}

module.exports = ConnectionsSchema
