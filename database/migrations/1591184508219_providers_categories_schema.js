'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProvidersCategoriesSchema extends Schema {
  up () {
    this.create('providers_categories', (table) => {
      table.increments()
      table
      .integer('provider_id')
      .unsigned()
      .references('id')
      .inTable('providers')
      .onUpdate('CASCADE')
      table
      .integer('category_id')
      .unsigned()
      .references('id')
      .inTable('categories')
      .onUpdate('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('providers_categories')
  }
}

module.exports = ProvidersCategoriesSchema
