'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.group(() => {
    Route.resource('products', 'ProductsController').except(['delete'])
    Route.post('products/:id/upload-image', 'ProductsController.uploadImage')
})
.prefix('v1')
.namespace('Products')
.middleware('auth')
Route.group(() => {
    Route.resource('products/:id/orders', 'OrdersController').only(['store'])
})
.prefix('v1')
.namespace('Products/Orders')
.middleware('auth')






