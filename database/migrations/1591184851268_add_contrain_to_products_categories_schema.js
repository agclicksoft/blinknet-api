'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddContrainToProductsCategoriesSchema extends Schema {
  up () {
    this.table('products_categories', (table) => {
      table
      .integer('category_id')
      .unsigned()
      .references('id')
      .inTable('categories')
      .onUpdate('CASCADE')
      .after('product_id')
    })
  }

  down () {
    this.table('add_contrain_to_products_categories', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddContrainToProductsCategoriesSchema
