'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class DeleteActiveFieldsSchema extends Schema {
  up () {
    this.table('buyers', (table) => {
      //table.dropForeign('activity_field_id')
      table.dropForeign('activity_field_id', 'buyers_activity_field_id _foreign');
      table.dropColumn('activity_field_id');
    }) 
    this.dropTable('activity_fields')  
  
    this.table('buyers', (table) => {
      table
      .integer('adress_id')
      .unsigned()
      .references('id')
      .inTable('adresses')
      .onUpdate('CASCADE')
      .after('id')
    })
    
  }

  down () {
    this.table('activity_fields', (table) => {
      
    })
  }
}

module.exports = DeleteActiveFieldsSchema
