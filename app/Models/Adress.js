'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Adress extends Model {
    providers () {
        return this.belongsToMany('App/Models/Provider')
        .pivotTable('operating_regions')
    }
}

module.exports = Adress
