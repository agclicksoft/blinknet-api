'use strict'

const Corporate = use('App/Models/Corporate')
const OperatingRegion = use('App/Models/OperatingRegion')
const Adress = use('App/Models/Adress')
const Category = use('App/Models/Category')
const Brand = use('App/Models/Brand')
const User = use('App/Models/User')

class CoporatesController {

    async show({params, pagination, request }){
        const results = await Buyer.query()
        .with('bayerproducts', (builder)=> {
            builder.with('products')
        })

    }
    async store({request, auth, response}) {
        const body = request.post()      
        const user = await auth.getUser()    
        const corporate = await user.corporate().create({})        
        user.merge({ email: body.email, cel: body.cel, name: body.name, cnpj: body.cnpj, company_name: body.company_name })
        user.currente_role = 'corporate'
        await user.save()
        var category
        for (const iterator of body.corporate.categories) {
            category = await Category.findOrCreate({ name: iterator.name })
            await corporate.categories().attach([category.id])
        }
        var brand
        for (const iterator of body.corporate.brands) {
            brand = await Brand.findOrCreate({ name: iterator.name })
            await corporate.brands().attach([brand.id])
        }
        await OperatingRegion.query().where('corporate_id', corporate.id).delete()
        var adress
        for (const i of body.corporate.adresses) {
            adress = await Adress.findOrCreate({
              parents: 'Brasil', state: i.state, city: i.city, neighborhood: i.neighborhood,
                //number: i.number, complement: i.complement, street: i.street
            })
             const operatingRegions = await corporate.operatingRegions().create({})
             
             await operatingRegions.adress().associate(adress)
        }
        const account = await User.query()
        .where('id', user.id)
        .with('provider', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('corporate', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('buyer', (builder) => {
          builder.with('categories')
        })
        .first()

        response.status(200).send({
            message: 'Dados atualizados com sucesso!',
            user: account
        })
    }

    async update({ request, response, params }) {
        const body = request.post()
        const corporate = await Corporate.findOrFail(params.id)
        const user = await corporate.user().fetch()
        user.merge({ email: body.email, cel: body.cel, name: body.name, cnpj: body.cnpj, company_name: body.company_name })
        user.currente_role = 'corporate'
        await user.save()
        var category
        for (const iterator of body.corporate.categories) {
            category = await Category.findOrCreate({ name: iterator.name })
            await corporate.categories().attach([category.id])
        }
        var brand
        for (const iterator of body.corporate.brands) {
            brand = await Brand.findOrCreate({ name: iterator.name })
            await corporate.brands().attach([brand.id])
        }
        await OperatingRegion.query().where('corporate_id', corporate.id).delete()
        var adress
        for (const i of body.corporate.adresses) {
            adress = await Adress.findOrCreate({
              parents: 'Brasil', state: i.state, city: i.city, neighborhood: i.neighborhood,
                //number: i.number, complement: i.complement, street: i.street
            })
             const operatingRegions = await corporate.operatingRegions().create({})
             
             await operatingRegions.adress().associate(adress)
        }
        const account = await User.query()
        .where('id', user.id)
        .with('provider', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('corporate', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('buyer', (builder) => {
          builder.with('categories')
        })
        .first()
        

        response.status(200).send({
            message: 'Dados atualizados com sucesso!',
            data: account
        })
    }
}

module.exports = CoporatesController