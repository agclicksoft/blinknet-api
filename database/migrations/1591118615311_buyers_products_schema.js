'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BuyersProductsSchema extends Schema {
  up () {
    this.create('buyers_products', (table) => {
      table.increments()
      table
      .integer('product_id')
      .unsigned()
      .references('id')
      .inTable('products')
      .onUpdate('CASCADE')  
      table
      .integer('buyer_id')
      .unsigned()
      .references('id')
      .inTable('buyers')
      .onUpdate('CASCADE')  
      table.timestamps()
    })
  }

  down () {
    this.drop('buyers_products')
  }
}

module.exports = BuyersProductsSchema
