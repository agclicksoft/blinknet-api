'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class BuyersSchema extends Schema {
  up () {
    this.create('buyers', (table) => {
      table.increments()
      table
      .integer('user_id ')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      
      table
      .integer('activity_field_id ')
      .unsigned()
      .references('id')
      .inTable('activity_fields')
      .onUpdate('CASCADE')
      table.string('tel', 20)
      table.timestamps()
    })
  }

  down () {
    this.drop('buyers')
  }
}

module.exports = BuyersSchema
