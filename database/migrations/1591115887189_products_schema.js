'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProductsSchema extends Schema {
  up () {
    this.create('products', (table) => {
      table.increments()
      table
      .integer('image_id')
      .unsigned()
      .references('id')
      .inTable('images')
      .onUpdate('CASCADE')
      table
      .integer('provider_id')
      .unsigned()
      .references('id')
      .inTable('providers')
      .onUpdate('CASCADE')
      table
      .integer('adress_id')
      .unsigned()
      .references('id')
      .inTable('adresses')
      .onUpdate('CASCADE')      
      table.string('publication_text', 254)
      table.boolean('receive_orders')
      table.timestamps()
    })
  }

  down () {
    this.table('products', (table) => {
      table.drop('products')
    })
  }
}

module.exports = ProductsSchema
