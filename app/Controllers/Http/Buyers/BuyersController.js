'use strict'

const Buyer = use('App/Models/Buyer')
const User = use('App/Models/User')
const Category = use('App/Models/Category')
const Adress = use('App/Models/Adress')

class BuyersController {
  
    async store({request, response, auth}) {
      const body = request.post()
      const user = await auth.getUser()
      const buyer = await user.buyer().create({tel: body.buyer.tel})     
      user.merge({ id: user.id, email: body.email, cel: body.cel, name: body.name, cnpj: body.cnpj, company_name: body.company_name })
      user.currente_role = 'buyer'
      await user.save()
      var category
      for (const iterator of body.buyer.categories) {
          category = await Category.findOrCreate({ name: iterator.name })
          await buyer.categories().attach([category.id])
      }     
      for (const i of body.buyer.adresses) {
          const adress = await Adress.findOrCreate({
              zip_code: i.zip_code, parents: 'Brasil', state: i.state, city: i.city, neighborhood: i.neighborhood,
              number: i.number, complement: i.complement, street: i.street
          })
          await buyer.adresses().associate(adress)
      }   
      const account = await User.query()
      .where('id', user.id)
      .with('provider', (builder) => {
        builder.with('categories')
        builder.with('adresses')
        builder.with('brands')
      })
      .with('corporate', (builder) => {
        builder.with('categories')
        builder.with('adresses')
        builder.with('brands')
      })
      .with('buyer', (builder) => {
        builder.with('categories')
        builder.with('adresses')
      })
      .first()      

      response.status(200).send({
          message: 'Dados atualizados com sucesso!',
          user: account
      })

    }

    async update({ request, response, params }) {
        const body = request.post()
        const buyer = await Buyer.findOrFail(params.id)
        buyer.merge({tel: body.tel})
        const user = await buyer.user().fetch()
        user.merge({ email: body.email, cel: body.cel, name: body.name, cnpj: body.cnpj, company_name: body.company_name })
        user.currente_role = 'buyer'
        await user.save()
        var category
        for (const iterator of body.buyer.categories) {
            category = await Category.findOrCreate({ name: iterator.name })
            await buyer.categories().attach([category.id])
        }     
        for (const i of body.buyer.adresses) {
            const adress = await Adress.findOrCreate({
                zip_code: i.zip_code, parents: 'Brasil', state: i.state, city: i.city, neighborhood: i.neighborhood,
                number: i.number, complement: i.complement, street: i.street
            })
            await buyer.adresses().associate(adress)
        }   
        const account = await User.query()
        .where('id', user.id)
        .with('provider', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('corporate', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('buyer', (builder) => {
          builder.with('categories')
          builder.with('adresses')
       
        })
        .first()      

        response.status(200).send({
            message: 'Dados atualizados com sucesso!',
            data: account
        })
    }
}

module.exports = BuyersController