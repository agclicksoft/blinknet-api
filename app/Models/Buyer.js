'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Buyer extends Model {
    orderProducts() {
        return this.hasMany('App/Models/BuyersProduct')        
    }
    user() {
        return this.belongsTo('App/Models/User')
    }
    connections() {
        return this.belongsToMany('App/Models/Provider')
        .pivotTable('connections')
    }
    categories () {
        return this.belongsToMany('App/Models/Category')
        .pivotTable('buyer_categories')
    }
    adresses() {
        return this.belongsTo('App/Models/Adress')
    }
}

module.exports = Buyer
