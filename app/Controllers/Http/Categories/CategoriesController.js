'use strict'

const Category = use('App/Models/Category')

class CategoriesController {

    async index() {
        const results = await Category.query().distinct(['id', 'name']).fetch()  
        return results
    }
}

module.exports = CategoriesController