'use strict'

const Product = use('App/Models/Product')
const Provider = use('App/Models/Provider')
const Adress = use('App/Models/Adress')
const Upload = use('App/Controllers/Http/Uploads/UploadsController')

class ProductsController {

    async index({pagination}) {
        const products = await Product.query()
        .with('image')
        .with('provider', (builder) => {
            builder.with('user', (builder) => {
                builder.with('image')
            })
        })
        .orderBy('created_at', 'desc')
        .paginate(pagination.page, pagination.perpage)

        return products
    } 


    async store({ request, auth, response }) {
        const body = request.post()
        const user = await auth.getUser()
        await user.load('provider')
        const provider = await Provider.find(user.$relations.provider.id)
        const adress = await Adress.findOrCreate({
            parents: body.adress.parents, state: body.adress.state, city: body.adress.state, neighborhood: body.adress.state
        })
        const product = await Product.create({
            publication_text: body.publication_text, receive_orders: body.receive_orders
        })

        await provider.products().save(product)
        await product.adress().associate(adress)
        
     
        return response.status(200).send({
            message: 'Produto cadastrado com sucesso',
            data: product
        })

    }

    async uploadImage({params, request}) {
        console.log('cheguei aqui 1')
        const product = await Product.findOrFail(params.id) 
        const up = new Upload()
        const image = await up.uploadImage({request})
        console.log('cheguei aqui 4')
        product.image().associate(image)  

    }
}

module.exports = ProductsController