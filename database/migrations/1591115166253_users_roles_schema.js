'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class UsersRolesSchema extends Schema {
  up () {
    this.create('users_roles', (table) => {
      table.increments()
      table
      .integer('user_id ')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      table.string('role')
      table.timestamps()
    })
  }

  down () {
    this.drop('users_roles')
  }
}

module.exports = UsersRolesSchema
