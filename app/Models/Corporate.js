'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Corporate extends Model {
    categories() {
        return this.belongsToMany('App/Models/Category')
        .pivotTable('corporates_categories')
    }
    operatingRegions() {
        return this.hasMany('App/Models/OperatingRegion')
    }
    brands() {
        return this.belongsToMany('App/Models/Brand')
        .pivotTable('corporates_brands')
    }
    products () {
        return this.hasMany('App/Models/Product')
    }
    user() {
        return this.belongsTo('App/Models/User')
    }
    connections() {
        return this.belongsToMany('App/Models/Buyer')
        .pivotTable('connections')
    }
    adresses () {
        return this.belongsToMany('App/Models/Adress')
        .pivotTable('operating_regions')
    }
}

module.exports = Corporate
