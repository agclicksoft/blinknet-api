'use strict'

const Provider = use('App/Models/Provider')
const Category = use('App/Models/Category')

class ConnectionsController {
    async index({auth, params}) {
        await Category.findOrFail(params.id)
        const user = await auth.getUser()
        const buyer = await user.buyer().fetch()
        const providers = await Provider.query()
        .whereHas('categories', (builder) => {
            builder.where('category_id', params.id)
        })
        .whereDoesntHave('connections', (builder) => {
            builder.where('buyer_id', '<>', buyer.id)
        })
        .with('user', (builder) => {
            builder.with('image')
        })
        .fetch()

        return providers
    }
}

module.exports = ConnectionsController