'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class ProviderBrand extends Model {
}

module.exports = ProviderBrand
