'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddCurrenteRoleToUserSchema extends Schema {
  up () {
    this.table('users', (table) => {
      table.string('currente_role', 20)
      .after('cnpj')
    })
  }

  down () {
    this.table('add_currente_role_to_users', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddCurrenteRoleToUserSchema
