'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ProviderBrandsSchema extends Schema {
  up () {
    this.create('provider_brands', (table) => {
      table.increments()
      table
      .integer('provider_id')
      .unsigned()
      .references('id')
      .inTable('providers')
      .onUpdate('CASCADE')  
      table
      .integer('brand_id')
      .unsigned()
      .references('id')
      .inTable('brands')
      .onUpdate('CASCADE')  
      table.timestamps()
    })
  }

  down () {
    this.drop('provider_brands')
  }
}

module.exports = ProviderBrandsSchema
