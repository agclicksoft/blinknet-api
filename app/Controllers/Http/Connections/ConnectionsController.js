'use strict'

const Connection = use('App/Models/Connection')
const Provider = use('App/Models/Provider')
const Buyer = use('App/Models/Buyer')
const Product = use('App/Models/Product')
const Mail = use('Mail')

class ConnectionController {

    async index({response, pagination, auth, request}) {
        const {search} = request.get()
        if (search == 'providers') {

            const user = await auth.getUser()
            const provider = await user.provider().fetch()
            
            const providers = await Buyer.query()
            .whereDoesntHave('connections', (builder) => {
                if (provider) {
                    builder.where('provider_id', '<>', provider.id)
                }              
            })
            .whereDoesntHave('user', (builder) => {
                builder.where('id', user.id)
            })
            .with('user', (builder) => {
                builder.with('image')
            })
            .paginate(pagination.page, pagination.perpage)    
    
            return providers

        }else {
            const user = await auth.getUser()
            const buyer = await user.buyer().fetch()
            
            const providers = await Provider.query()
            .whereDoesntHave('connections', (builder) => {
                if (buyer)
                    builder.where('buyer_id', '<>', buyer.id)
            })
            .whereDoesntHave('user', (builder) => {
                builder.where('id', user.id)
            })
            .with('user', (builder) => {
                builder.with('image')
            })
            .paginate(pagination.page, pagination.perpage)    
    
            return providers
        }
     
    }

    async store({ request, response }) {     
        const {provider_id, buyer_id, amount, product_id, send_control} = request.all()
        const provider = await Provider.findOrFail(provider_id)
        const buyer = await Buyer.findOrFail(buyer_id)
        const userProvider = await provider.user().fetch()
        const userBuyer = await buyer.user().fetch()
        const result = await Connection.findOrCreate({provider_id, buyer_id, send_control})        
      
        
        result.accept= false;
        await result.save()        
        if (product_id) {
            const product = await Product.query()
            .where('id', product_id)
            .with('categories')
            .first()
            if (amount > 0 && product.receive_orders) {
                await Mail.send('send-orders', {amount, product, userProvider, userBuyer}, (message) => {
                    message
                      .from(userProvider.email)
                      .to('marco.lima@clicksoft.com.br') //userProvider.email
                  })    
            }
        }
    
     
        return response.status(200).send({
            message: 'Envio da conexão efetuada',
            data: result
        })
    }

    async update({params, request, response}) {
        const connection = await Connection.findOrFail(params.id)
        const {accept} = request.all()
        connection.accept= accept
        connection.save()

        return response.status(200).send({
            message: 'Convite a conexão aceito',
            data: connection
        })
        
    }

    async destroy({params, response}) {
        try {
            await Connection.query().where('id', params.id).delete()
        } catch (error) {
            return response.status(409).send({
                error
            })
        }
        
    }

    async getBuyersConnections({auth, response, request}) {
        // retorna compradores que não tenham coneções com o fornecedor
        const {category} = request.get()
        const user = await auth.getUser()
        const provider = await user.provider().fetch()
        if (!provider) return response.status(401).send({message: 'perfil de fornecedor não encontrado'})
        const buyers = await Buyer.query()
        .whereDoesntHave('connections', (builder) => {
            builder.where('provider_id', provider.id)
        })
        .whereHas('categories', (builder) => {
            if (category)
                builder.where('name', category)
        })
        .with('user', (builder) => {
            builder.with('image')
        })
        .fetch()
        return buyers
    }
    async getProvidersConnections({auth, response, request}) {
        // retorna fornecedores que não tenham coneções com o comprodor
        const user = await auth.getUser()
        const {category} = request.get()
        const buyer = await user.buyer().fetch()
        if (!buyer) return response.status(401).send({message: 'perfil de comprador não encontrado'})
        const providers = await Provider.query()
        .whereDoesntHave('connections', (builder) => {
            builder.where('buyer_id', buyer.id)
        })
        .whereHas('categories', (builder) => {
            if (category)
                builder.where('name', category)
        })
        .with('user', (builder) => {
            builder.with('image')
        })
        .with('brands')
        .fetch()
        return providers
    }

}

module.exports = ConnectionController