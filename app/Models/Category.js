'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Category extends Model {
    providers() {
        this.belongsToMany('App/Models/Provider')
        .pivotTable('providers_categories')
    }
    products () {
        this.belongsToMany('App/Models/Product')
        .pivotTable('products_categories')
    }
    buyers () {
        this.belongsToMany('App/Models/Buyer')
        .pivotTable('buyer_categories')
    }

}

module.exports = Category
