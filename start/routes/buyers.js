'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.group(() => {
    Route.resource('buyers', 'BuyersController').only(['update', 'store'])
})
    .prefix('v1')
    .namespace('Buyers')
    .middleware('auth')

Route.group(() => {
    Route.resource('buyers/:id/orders', 'OrdersController').only(['index'])
})
    .prefix('v1')
    .namespace('Buyers/Orders')
    .middleware('auth')

//Connections prividers
Route.group(() => {

    Route.resource('buyers/:id/connections', 'ConnectionsController').only(['index'])

}).prefix('v1')
    .namespace('Buyers/Connections')
    .middleware('auth')