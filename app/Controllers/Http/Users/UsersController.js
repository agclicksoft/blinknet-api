'use strict'

const User = use('App/Models/User')
const Upload = use('App/Controllers/Http/Uploads/UploadsController')

class UsersController {
    async uploadImage({params, request, response}) {
        const user = await User.findOrFail(params.id) 
        const up = new Upload()
        const image = await up.uploadImage({request})
        user.image().associate(image)

        const account = await User.query()
        .where('id', user.id)
        .with('provider', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('corporate', (builder) => {
          builder.with('categories')
          builder.with('adresses')
          builder.with('brands')
        })
        .with('buyer', (builder) => {
          builder.with('categories')
        })
        .first()

        return response.status(200).send({
            message: 'Foto do perfil atualizada com sucesso',
            data: account
        })
    }
}

module.exports = UsersController