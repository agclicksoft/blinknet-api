'use strict'

const Provider = use('App/Models/Provider')
const BuyersProduct = use('App/Models/BuyersProduct')

class OrdersController {

    async index({params, pagination}) {
        const provider = await Provider.findOrFail(params.id)
        const results = await BuyersProduct.query()
        .whereHas('product', (builder) => {
            builder.whereHas('provider', (builder) => {
                builder.where('id', provider.id)
            })
        })
        .with('product', (builder) => {
            builder.with('image')
        })
        .with('buyer', (builder) => {
            builder.with('user')
        })
        .paginate(pagination.page, pagination.perpage)

        return results
    }

}

module.exports = OrdersController