'use strict'

const Buyer = use('App/Models/Buyer')
const BuyersProduct = use('App/Models/BuyersProduct')

class OrdersController {
    async index({params, auth, pagination}) {
        await Buyer.findOrFail(params.id)
        const user = await auth.getUser()
        const buyer = await user.buyer().fetch()
        const orders = await BuyersProduct.query()
        .where({buyer_id: buyer.id})
        .with('product', (builder) => {
            builder.with('provider', (builder) => {
                builder.with('user')
            })
        })
        .paginate(pagination.page, pagination.perpage)
        return orders
    }
}

module.exports = OrdersController