'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CorporateSchema extends Schema {
  up () {
    this.create('corporates', (table) => {
      table.increments()
      table
      .integer('user_id ')
      .unsigned()
      .references('id')
      .inTable('users')
      .onUpdate('CASCADE')
      table.timestamps()
    })
  }

  down () {
    this.drop('corporates')
  }
}

module.exports = CorporateSchema
