'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddContraintToRegionsSchema extends Schema {
  up () {
    this.table('operating_regions', (table) => {
      table
      .integer('adress_id')
      .unsigned()
      .references('id')
      .inTable('adresses')
      .onUpdate('CASCADE')
      .after('id')
    })
  }

  down () {
    this.table('add_contraint_to_regions', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddContraintToRegionsSchema
