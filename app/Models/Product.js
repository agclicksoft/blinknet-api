'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Product extends Model {
    image() {
        return this.belongsTo('App/Models/Image')
    }
    provider() {
        return this.belongsTo('App/Models/Provider')
    }
    adress() {
        return this.belongsTo('App/Models/Adress')
    }
    categories() {
        return this.belongsToMany('App/Models/Category')
        .pivotTable('products_categories')
    }
}

module.exports = Product
