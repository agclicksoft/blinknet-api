'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.0/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

/**
 * Import Auth Routes
 */
require('./auth')
/**
 * Import Providers Routes
 */
require('./providers')
/**
 * Import Products Routes
 */
require('./products')
/**
 * Import Products Routes
 */
require('./users')
/**
 * Import Connections Routes
 */
require('./connections')
/**
 * Import Connections Routes
 */
require('./buyers')
/**
 * Import Categories Routes
 */
require('./categories')
/**
 * Import Categories Routes
 */
require('./corporates')
