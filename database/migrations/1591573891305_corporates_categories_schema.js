'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class CorporatesCategoriesSchema extends Schema {
  up () {
    this.create('corporates_categories', (table) => {
      table.increments()
      table
      .integer('corporate_id')
      .unsigned()
      .references('id')
      .inTable('corporates')
      .onUpdate('CASCADE')
      table
      .integer('category_id')
      .unsigned()
      .references('id')
      .inTable('categories')
      .onUpdate('CASCADE')
      table.timestamps()
    })
  
  }

  down () {
    this.drop('corporates_categories')
  }
}

module.exports = CorporatesCategoriesSchema
