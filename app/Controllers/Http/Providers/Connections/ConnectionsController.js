'use strict'

const Connection = use('App/Models/Connection')
const Provider = use('App/Models/Provider')

class ConnectionsController {

    async index({params, auth}) {
        const provider = await Provider.findOrFail(params.id)
                   
        
        const connections = await Connection.query()
        .where({provider_id: provider.id})
     
        .with('buyer', (builder) => {
            builder.with('user', (builder) => {
                builder.with('image')
            })
        })
        .with('provider', (builder) => {
            builder.with('user', (builder) => {
                builder.with('image')
            })
        })
        .fetch()

        return connections
    }
}

module.exports = ConnectionsController