'use strict'

const Buyer = use('App/Models/Buyer')
const Provider = use('App/Models/Provider')
const Product = use('App/Models/Product')

class OrdersController {

    async index({auth}) {
        const user = await auth.getUser()
        const buyer = await user.buyer()
        const orders = await buyer.orderProducts()
        return orders
        
    }

    async store({ response, auth, params}) {
        const product = await Product.findOrFail(params.id)
        const user = await auth.getUser()
        const buyer = await user.buyer().fetch()        
        
        const order = await buyer.orderProducts().create({product_id : product.id})

        return response.status(200).send({
            message: 'Nova solicitação enviada!',
            data: order
        })
    }

}

module.exports = OrdersController