'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddContraintToOperationsRegionsSchema extends Schema {
  up () {
    this.table('operating_regions', (table) => {
      table
      .integer('corporate_id')
      .unsigned()
      .references('id')
      .inTable('corporates')
      .onUpdate('CASCADE')
      .after('id')
    })
  }

  down () {
    this.table('operating_regions', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddContraintToOperationsRegionsSchema
