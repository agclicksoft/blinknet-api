'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddZipCodeInAdressSchema extends Schema {
  up () {
    this.table('adresses', (table) => {
      table.string('zip_code')
      .after('id')
    })
  }

  down () {
    this.table('add_zip_code_in_adresses', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddZipCodeInAdressSchema
