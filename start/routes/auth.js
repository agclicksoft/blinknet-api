'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')
Route.group(() => {
  Route.post('register', 'AuthController.register')

  Route.post('authenticate', 'AuthController.login')


  Route.post('refresh', 'AuthController.refresh')


  Route.post('logout', 'AuthController.logout')
  Route.post('account', 'AuthController.account')
  Route.post('change-password', 'AuthController.changePassword')
  Route.put('update-role', 'AuthController.updateRole')


  Route.get('reset-password', 'AuthController.remember').as('auth.remember')
  Route.post('reset-password', 'AuthController.forgot').as('auth.forgot')
  Route.put('reset-password', 'AuthController.reset').as('auth.reset')
  // .validator('Clients/ClientRefreshToken')
})
  .prefix('v1/auth')
  .namespace('Auth')
