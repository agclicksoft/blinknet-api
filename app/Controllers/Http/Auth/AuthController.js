'use strict'

const User = use('App/Models/User')
const Database = use('Database')
const Env = use('Env')


class AuthController {
  async register({ request, response, auth }) {
    const trx = await Database.beginTransaction()

    try {
      const { name, email, password, role, cel }= request.all()

      const findUser = await User.findBy('email', email)
      if (findUser) {
        return response.status(409).send({data :'email já cadastrado'})
      }

      const user = await User.create({ name, email, password, cel }, trx)
      // commita a transaction
      await trx.commit()
      switch (role) {
          case 'buyer':
            await user.buyer().create({})                  
              break;
          case 'corporate':
            await user.corporate().create({})      
              break;
          case 'provider':
            await user.provider().create({})      
              break;
      
          default:
              break;
      }
      await user.usersRoles().create({role})      
      let data = await auth.withRefreshToken().attempt(email, password)
      user.currente_role = role
      user.save()
      await user.load(role)
      return response.status(201).send({
           message : 'Usuário cadastrado com sucesso!',
           data: data,
           user: user
      })
    } catch (e) {
      await trx.rollback()
      return response.status(400).send({
        error: 'Erro ao realizar cadastro',
        message: e.message
      })
    }
  }

  async login({ request, response, auth }) {
    
    const { email, password } = request.only(['email', 'password'])
  
    let data = await auth.withRefreshToken().attempt(email, password)

    const user = await User.findByOrFail('email', email)
    await user.load('provider')
    await user.load('buyer')
   
    const account = await User.query()
    .where('id', user.id)
    .with('provider', (builder) => {
      builder.with('categories')
      builder.with('adresses')
      builder.with('brands')
    })
    .with('corporate', (builder) => {
      builder.with('categories')
      builder.with('adresses')
      builder.with('brands')
    })
    .with('buyer', (builder) => {
      builder.with('categories')
    })
    .first()

 
    
    return response.send({ data, user: account })
  }

  async account({auth}) {
    const user = await auth.getUser()
    const account = await User.query()
    .where('id', user.id)
    .with('provider', (builder) => {
      builder.with('categories')
      builder.with('adresses')
      builder.with('brands')
    })
    .with('buyer')
    .with('corporate')
    .first()
    return account
  }


  async refresh({ request, response, auth }) {
    var refresh_token = request.input('refresh_token')
    console.log(request)

    if (!refresh_token) {
      refresh_token = request.header('refresh_token')
    }

    const data = await auth
      .newRefreshToken()
      .generateForRefreshToken(refresh_token)

    const user = await auth
      .getUser()

    return response.send({ data: data, user })
  }

  async logout({ request, response, auth }) {
    let refresh_token = request.input('refresh_token')

    if (!refresh_token) {
      refresh_token = request.header('refresh_token')
    }

    const loggedOut = await auth
      .authenticator('jwt')
      .revokeTokens([refresh_token], true)

    return response.status(204).send({})
  }

  
  async updateRole({request, auth, response}){
    const user = await auth.getUser()
    user.merge(request.only(['currente_role']))
    user.save()

    const account = await User.query()
    .where('id', user.id)
    .with('provider', (builder) => {
      builder.with('categories')
      builder.with('adresses')
      builder.with('brands')
    })
    .with('corporate', (builder) => {
      builder.with('categories')
      builder.with('adresses')
      builder.with('brands')
    })
    .with('buyer', (builder) => {
      builder.with('categories')
      builder.with('adresses')
    })
    .first()

    return response.status(200).send({
      message: 'Rota atualizada com sucesso!',
      data: account
    })

  }


  async forgot({ request, response }) {
    const user = await User.findByOrFail('email', request.input('email'))
    const req = request
    try {
      /**
       * Invalida qualquer outro token que tenha sido gerado anteriormente
       */
      await PasswordReset.query()
        .where('email', user.email)
        .delete()

      /**
       * gera um novo token para reset da senha
       */
      const reset = await PasswordReset.create({ email: user.email })

      // Envia um novo e-mail para o Usuário, com um token para que ele possa alterar a senha
      await Mail.send(
        'emails.reset',
        { user, reset, referer: req.request.headers['referer'] },
        message => {
          message
            .to(user.email)
            .from(Env.get('DO_NOT_ANSWER_EMAIL'))
            .subject('Solicitação de Alteração de Senha')
        }
      )

      return response.status(201).send({
        message:
          'Um e-mail com link para reset foi enviado para o endereço informado!'
      })
    } catch (error) {
      return response.status(400).send({
        message: 'Ocorreu um erro inesperado ao executar a sua solicitação!'
      })
    }
  }

  async remember({ request, response }) {
    const reset = await PasswordReset.query()
      .where('token', request.input('token'))
      .where('expires_at', '>=', new Date())
      .firstOrFail()

    return response.send(reset)
  }
  async changePassword({ request, auth, response}) {
      const user = await auth.getUser()
      user.merge(request.only(['password']))
      await user.save()
      return response.status(200).send({
        message: 'Senha alterada com sucesso'
      })


  }

  async reset({ request, response }) {
    const { email, password } = request.all()
    const user = await User.findByOrFail('email', email)
    try {
      user.merge({ password })
      await user.save()
      /**
       * Invalida qualquer outro token que tenha sido gerado anteriormente
       */
      await PasswordReset.query()
        .where('email', user.email)
        .delete()
      return response.send({ message: 'Senha alterada com sucesso!' })
    } catch (error) {
      return response
        .status(400)
        .send({ message: 'Não foi possivel alterar a sua senha!' })
    }
  }
}

module.exports = AuthController
