'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddSendControlToConnectionsSchema extends Schema {
  up () {
    this.table('connections', (table) => {
      table.bigInteger('send_control')
      .after('corporate_id')
    })
  }

  down () {
    this.table('connections', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddSendControlToConnectionsSchema
