'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AdressesSchema extends Schema {
  up () {
    this.create('adresses', (table) => {
      table.increments()
      table.string('parents', 100)
      table.string('state', 100)
      table.string('city', 100)
      table.string('neighborhood', 100)
      table.timestamps()
    })
  }

  down () {
    this.drop('adresses')
  }
}

module.exports = AdressesSchema
