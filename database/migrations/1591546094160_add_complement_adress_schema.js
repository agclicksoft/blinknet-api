'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class AddComplementAdressSchema extends Schema {
  up () {
    this.table('adresses', (table) => {
      table.string('number', 25).after('neighborhood')
      table.string('complement', 200).after('number')
      table.string('street',200).after('complement')
    })
  }

  down () {
    this.table('adresses', (table) => {
      // reverse alternations
    })
  }
}

module.exports = AddComplementAdressSchema
